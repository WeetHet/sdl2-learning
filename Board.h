#pragma once

#include <array>
class Board {
private:
    std::array<std::array<int, 3>, 3> b;
public:
    Board();
    bool check_move(int x, int y, int sign) const;
    void move(int x, int y, int sign);
    int is_win() const;
    int get_cell(int x, int y) const;
};

