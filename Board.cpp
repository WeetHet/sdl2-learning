#include "Board.h"
#include <array>
#include <ranges>

Board::Board() {
  for (auto &e : b) {
    e.fill(-1);
  }
}

bool Board::check_move(int x, int y, [[maybe_unused]] int sign) const { return b[x][y] == -1; }

void Board::move(int x, int y, int sign) { b[x][y] = sign; }

int Board::is_win() const {
  // check horizontal
  for (std::size_t row = 0; row < b.size(); row += 1) {
    bool is_win_row = true;
    for (std::size_t col = 0; col < b[row].size(); col += 1) {
      if (get_cell(row, col) == -1 || get_cell(row, col) != get_cell(row, 0)) {
        is_win_row = false;
      }
    }
    if (is_win_row) {
      return get_cell(row, 0);
    }
  }

  // check vertical
  for (std::size_t col = 0; col < b[0].size(); col += 1) {
    bool is_win_col = true;
    for (std::size_t row = 0; row < b.size(); row += 1) {
      if (get_cell(row, col) == -1 || get_cell(row, col) != get_cell(0, col)) {
        is_win_col = false;
      }
    }
    if (is_win_col) {
      return get_cell(0, col);
    }
  }

  // check diagonal tl-br
  bool is_win_diagonal_first = true;
  for (std::size_t row = 0, col = 0; row < b.size() && col < b[row].size();
       row += 1, col += 1) {
    if (get_cell(row, col) == -1 || get_cell(row, col) != get_cell(0, 0)) {
      is_win_diagonal_first = false;
    }
  }

  if (is_win_diagonal_first) {
    return get_cell(0, 0);
  }

  // check diagonal tr-bl
  bool is_win_diagonal_second = true;
  for (int row = 0, col = b[0].size() - 1; row < std::ssize(b) && col >= 0;
       row += 1, col -= 1) {
    if (get_cell(row, col) == -1 ||
        get_cell(row, col) != get_cell(0, b[0].size() - 1)) {
      is_win_diagonal_second = false;
    }
  }

  if (is_win_diagonal_second) {
    return get_cell(0, b[0].size() - 1);
  }

  bool is_draw = true;
  for (size_t row = 0; row < b.size(); row += 1) {
    for (size_t col = 0; col < b[0].size(); col += 1) {
      if (get_cell(row, col) == -1) {
        is_draw = false;
      }
    }
  }

  return is_draw? 3: -1;
}

int Board::get_cell(int x, int y) const { return b[x][y]; }
