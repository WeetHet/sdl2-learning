let 
  pkgs = import <nixpkgs> { };
in
pkgs.gcc12Stdenv.mkDerivation {
  name = "lesson-sdl2";
  nativeBuildInputs = with pkgs; [ cmake meson ninja SDL2 SDL2_ttf SDL2_image pkg-config ];

  src = ./.;

  configurePhase = ''
    meson setup build
  '';

  buildPhase = ''
    meson compile -C build
  '';

  installPhase = ''
    mkdir $out
    DESTDIR=$out meson install -C build 
  '';
}