#pragma once

#define SDL_MAIN_HANDLED
#include "Board.h"
#include "SDL_render.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <optional>

struct Texture {
    SDL_Texture* texture;
    int texture_w;
    int texture_h;
};

class BoardView {
private:
    static const int SCREEN_WIDTH = 640;
    static const int SCREEN_HEIGHT = 480;
    static const int SCREEN_FPS = 60;
    static const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

    Board* b;
    SDL_Window* window;
    SDL_Renderer* renderer;
    Texture x_texture;
    Texture o_texture;

    int tile_size;
private:
    void show_board() const;
    std::optional<Texture> load_texture(const char* path);
    TTF_Font* load_font(const char* path, int size) const;
    void try_set_tile_size_from(const char* path);
    void render_text(const char* text) const;
    bool finish;
public:
    BoardView(Board* board);
    ~BoardView();
    void do_game_loop();
};
