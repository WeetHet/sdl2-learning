#include <array>
#include <iostream>

#include "Board.h"
#include "rapidcheck/Check.h"
#include "rapidcheck/Gen.hpp"
#include "rapidcheck/gen/Arbitrary.hpp"
#include "rapidcheck/gen/Build.h"
#include "rapidcheck/gen/Container.h"
#include "rapidcheck/gen/Numeric.h"
#include "rapidcheck/gen/Select.h"
#include "rapidcheck/gen/Transform.h"

struct Turn {
  int row;
  int col;
  int sign;
};

namespace rc {
template <> struct Arbitrary<Turn> {
  static Gen<Turn> arbitrary() {
    return gen::build<Turn>(gen::set(&Turn::row, gen::inRange(0, 3)),
                            gen::set(&Turn::col, gen::inRange(0, 3)),
                            gen::set(&Turn::sign, gen::element(0, 1)));
  }
};
} // namespace rc

struct Diagonal {
  std::array<Turn, 3> turns;
};

namespace rc {
template <> struct Arbitrary<Diagonal> {
  static Gen<Diagonal> arbitrary() {
    auto left_diagonal = [](int sign) {
      return gen::map(gen::unique<std::array<int, 3>>(gen::inRange(0, 3)),
                      [sign](const std::array<int, 3> &pos) {
                        const auto [f, s, t] = pos;
                        return std::array{Turn{f, f, sign}, Turn{s, s, sign},
                                          Turn{t, t, sign}};
                      });
    };

    auto right_diagonal = [](int sign) {
      return gen::map(gen::unique<std::array<int, 3>>(gen::inRange(0, 3)),
                      [sign](const std::array<int, 3> &pos) {
                        const auto [f, s, t] = pos;
                        return std::array{Turn{f, 2 - f, sign},
                                          Turn{s, 2 - s, sign},
                                          Turn{t, 2 - t, sign}};
                      });
    };

    enum class DiagonalType { Left, Right };

    auto generate_diagonal_array =
        gen::join(gen::map(gen::element(0, 1), [&](int sign) {
          return gen::join(
              gen::map(gen::element(DiagonalType::Left, DiagonalType::Right),
                       [&](auto diagonal_type) {
                         if (diagonal_type == DiagonalType::Left) {
                           return left_diagonal(sign);
                         } else {
                           return right_diagonal(sign);
                         }
                       }));
        }));

    return gen::build<Diagonal>(
        gen::set(&Diagonal::turns, generate_diagonal_array));
  }
};
} // namespace rc

void showValue(const Turn &turn, std::ostream &os) {
  auto [p, q, t] = turn;
  os << "Turn: " << p << " " << q << " of player " << t;
}

void showValue(const Diagonal &diagonal, std::ostream &os) {
  for (auto turn : diagonal.turns) {
    showValue(turn, os);
    os << '\n';
  }
}

int main() {
  // check same move
  rc::check([](const Turn &turn) {
    Board b;
    b.move(turn.row, turn.col, turn.sign);
    return b.check_move(turn.row, turn.col, turn.sign) == false;
  });

  // check diagonal win
  rc::check([](const Diagonal &diagonal) {
    Board b;
    for (auto [p, q, t] : diagonal.turns) {
      b.move(p, q, t);
    }

    return b.is_win() == diagonal.turns[0].sign;
  });
}