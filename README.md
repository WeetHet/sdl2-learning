# This is currently a project for the C++ SDL2 course

## How to use?
1. Install nix:
```shell
sh <(curl -L https://nixos.org/nix/install)
```

If `nix-shell` is not found, add this to `.bashrc`/`.zshrc`: 
```shell
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
```

2. Clone project:
```shell
git clone https://gitlab.com/WeetHet/sdl2-leaning.git
```

3. Setup development environment with `nix-shell`

4. Build with `nix-build`

## Setup VSCode to use with this repository
### Install extensions: 
[clangd](https://marketplace.visualstudio.com/items?itemName=llvm-vs-code-extensions.vscode-clangd)

[Nix Environment Selector](https://marketplace.visualstudio.com/items?itemName=arrterian.nix-env-selector)

#### Optionally:
[Meson support](https://marketplace.visualstudio.com/items?itemName=mesonbuild.mesonbuild)

[Nix language support](https://marketplace.visualstudio.com/items?itemName=bbenoist.Nix)

[CodeLLDB if you need debugging](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb)
