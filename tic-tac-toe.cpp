#include "Board.h"
#include "BoardView.h"

int main() {
  Board b;
  BoardView bw(&b);
  bw.do_game_loop();
  return 0;
}
