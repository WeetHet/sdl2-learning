let 
  pkgs = import <nixpkgs> { };
in 
pkgs.gcc12Stdenv.mkDerivation {
  name = "lesson-sdl2";
  buildInputs = with pkgs; [ cmake meson ninja SDL2 SDL2_ttf SDL2_image pkg-config clang-tools ];

  shellHook = ''
    if [ ! -d build-debug ]; then
      meson setup build-debug
      rm -rf compile_commands.json && ln -s build-debug/compile_commands.json
    fi
  '';
}