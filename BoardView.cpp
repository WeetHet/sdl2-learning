#include "BoardView.h"
#include "SDL_rect.h"
#include "SDL_render.h"
#include "SDL_surface.h"
#include <algorithm>
#include <iostream>
#include <optional>
#include <stdlib.h>

constexpr int PADDING = 6;
constexpr int TEXT_PADDING = 80;

TTF_Font *BoardView::load_font(const char *path, int size) const {
  TTF_Font *font = TTF_OpenFont(path, size);
  if (font == NULL) {
    std::cerr << "Error: failed to load font " << path
              << " SDL_image error: " << TTF_GetError() << "\n";
    return NULL;
  }
  return font;
}

void BoardView::try_set_tile_size_from(const char *path) {
  SDL_Surface *surface = IMG_Load(path);
  if (surface == NULL) {
    std::cerr << "Error: unable to load image " << path
              << " SDL_image error: " << IMG_GetError() << "\n";
  }

  tile_size = std::max({tile_size, surface->w, surface->h});
  SDL_FreeSurface(surface);
}

BoardView::BoardView(Board *board) : b(board) {
  // init library
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cerr << "Error: SDL could not initialize. SDL_Error: "
              << SDL_GetError() << "\n";
    exit(1);
  }

  tile_size = 0;
  try_set_tile_size_from("x.png");
  try_set_tile_size_from("o.png");

  // create window and frame render
  window = SDL_CreateWindow(
      "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      PADDING + 3 * (PADDING + tile_size),
      TEXT_PADDING + PADDING + 3 * (PADDING + tile_size), SDL_WINDOW_SHOWN);
  if (window == NULL) {
    std::cerr << "Error: window could not be created. SDL_Error: "
              << SDL_GetError() << "\n";
    exit(1);
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (renderer == NULL) {
    std::cerr << "Error: renderer could not be created. SDL Error: "
              << SDL_GetError() << "\n";
    exit(1);
  }

  int img_flags = IMG_INIT_PNG;
  if (!(IMG_Init(img_flags) & img_flags)) {
    std::cerr << "Error: SDL image could not initialize. SDL_image error: "
              << IMG_GetError() << "\n";
    exit(1);
  }
  if (TTF_Init() == -1) {
    std::cerr << "SDL_ttf could not initialize. SDL_ttf error: "
              << TTF_GetError() << "\n";
    exit(1);
  }

  // x.png drawn by myself in draw.io =)
  auto opt_x_texture = load_texture("x.png");
  if (!opt_x_texture.has_value()) {
    std::cerr << "Error: failed to load x.png texture.\n";
    exit(1);
  }
  x_texture = opt_x_texture.value();

  auto opt_o_texture = load_texture("o.png");
  if (!opt_o_texture.has_value()) {
    std::cerr << "Error: failed to load o.png texture.\n";
    exit(1);
  }
  o_texture = opt_o_texture.value();

  tile_size = std::max({x_texture.texture_h, x_texture.texture_w,
                        o_texture.texture_h, o_texture.texture_w});
}

BoardView::~BoardView() {
  SDL_DestroyTexture(x_texture.texture);

  // delete everything
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  IMG_Quit();
  SDL_Quit();
}

void BoardView::render_text(const char *text) const {
  auto font = load_font("FiraCode-Regular.ttf", 28);
  if (font == NULL) {
    std::cerr << "Error: failed to load font.\n";
    exit(1);
  }

  SDL_Color text_color = {0, 0, 0, 0};
  SDL_Surface *surface = TTF_RenderText_Solid(font, text, text_color);
  SDL_Rect renderer_clip = {0, 0, surface->w, surface->h};

  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);

  int result = SDL_RenderCopy(renderer, texture, nullptr, &renderer_clip);
  std::cerr << result << '\n';
}

std::optional<Texture> BoardView::load_texture(const char *path) {
  SDL_Surface *surface = IMG_Load(path);
  if (surface == NULL) {
    std::cerr << "Error: unable to load image " << path
              << " SDL_image error: " << IMG_GetError() << "\n";
    return std::nullopt;
  }

  int surface_w = surface->w, surface_h = surface->h;

  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);
  if (texture == NULL) {
    std::cerr << "Error: unable to create texture from " << path
              << " SDL error: " << SDL_GetError() << "\n";
    return std::nullopt;
  }

  Texture result{texture, surface_w, surface_h};
  return result;
}

void BoardView::show_board() const {
  SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);

  int window_width = 3 * (tile_size + PADDING);
  int window_height = 3 * (tile_size + PADDING);
  // draw horizontal lines
  for (int line_row = 0; line_row < 4; line_row += 1) {
    int line_y = int((double(line_row) / 3.0) * window_width);

    SDL_RenderDrawLine(renderer, 0, TEXT_PADDING + line_y,
                       3 * (tile_size + PADDING), TEXT_PADDING + line_y);
  }

  // draw vertical lines
  for (int line_col = 0; line_col < 4; line_col += 1) {
    int line_x = int((double(line_col) / 3.0) * window_height);

    SDL_RenderDrawLine(renderer, line_x, TEXT_PADDING, line_x,
                       TEXT_PADDING + 3 * (tile_size + PADDING));
  }

  // draw pieces
  for (int row = 0; row < 3; row += 1) {
    for (int col = 0; col < 3; col += 1) {
      if (b->get_cell(row, col) == 0) {
        SDL_Rect renderer_clip = {PADDING + (tile_size + PADDING) * col,
                                  TEXT_PADDING + PADDING +
                                      (tile_size + PADDING) * row,
                                  x_texture.texture_w, x_texture.texture_h};
        SDL_RenderCopy(renderer, x_texture.texture, nullptr, &renderer_clip);
      } else if (b->get_cell(row, col) == 1) {
        SDL_Rect renderer_clip = {PADDING + (tile_size + PADDING) * col,
                                  TEXT_PADDING + PADDING +
                                      (tile_size + PADDING) * row,
                                  o_texture.texture_w, o_texture.texture_h};
        SDL_RenderCopy(renderer, o_texture.texture, nullptr, &renderer_clip);
      }
    }
  }

  if (finish) {
    auto win = b->is_win();
    std::cerr << win << '\n';
    if (win == 0) {
      render_text("Crosses win");
    } else if (win == 1) {
      render_text("Circles win");
    } else {
      render_text("Tie");
    }
  }
}

void BoardView::do_game_loop() {
  bool quit = false;
  finish = false;
  int cur_sign = 0;

  while (!quit) {
    int start_ticks = SDL_GetTicks();

    SDL_Event e;
    while (SDL_PollEvent(&e) != 0) {
      if (e.type == SDL_QUIT)
        quit = true;
      else if (e.type == SDL_MOUSEBUTTONDOWN && !finish) {
        int x, y;
        SDL_GetMouseState(&x, &y);
        std::cout << "Mouse down (" << x << "," << y << ")\n";

        int col = x / (tile_size + PADDING),
            row = (y - TEXT_PADDING) / (tile_size + PADDING);
        if (b->check_move(row, col, cur_sign)) {
          b->move(row, col, cur_sign);
          cur_sign ^= 1;

          if (int win = b->is_win(); win != -1) {
            finish = true;
          }
        }
      }
    }

    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderClear(renderer);

    show_board();

    SDL_RenderPresent(renderer);
    int frame_ticks = SDL_GetTicks() - start_ticks;
    if (frame_ticks < SCREEN_TICKS_PER_FRAME)
      SDL_Delay(SCREEN_TICKS_PER_FRAME - frame_ticks);
  }
}
